//Чому для роботи з input не рекомендується використовувати клавіатуру?
//input спрацьовує у результаті будь якого введення що міняє значення, при роботі з клавіатурою може не спрацьовувати
// оскільки на клавіатурі можливі дії які не міняють значення. Події клавіатури повинні використовуватися тільки за призначенням – для клавіатури. Наприклад, щоб реагувати на гарячі або спеціальні кнопки.



const listKey = document.querySelector('.btn-wrapper');
function pushKey(e) {let elemForKey = findElementForKey(e.key.toUpperCase()); if (!elemForKey) return;
   unpaintAllKey();
    paintKey(elemForKey);
}
document.addEventListener('keydown', pushKey);
paintKey()
console.log(...listKey.children)
function findElementForKey(keyValue) {return [...listKey.children].find(elem => elem.textContent.toUpperCase() === keyValue);
}

function paintKey(elemForKey) {  elemForKey.style.backgroundColor = 'blue';
}
//
function unpaintAllKey() {  [...listKey.children].forEach((elem) => {
      elem.removeAttribute('style');
  });
 }

